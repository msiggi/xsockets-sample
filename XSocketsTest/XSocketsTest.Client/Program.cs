﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSockets.Client40;

namespace XSocketsTest.Client
{
    class Program
    {
        static void Main(string[] args)
        {
             // var conn = new XSocketClient("ws://127.0.0.1:59799/", "http://localhost", "Chat"); // not working
          //  var conn = new XSocketClient("ws://localhost:59799", "http://localhost", "Chat");
            
            conn.Open();

            conn.Controller("Chat").Invoke("ChatMessage", new { Text = "this is a Testmessage" });

            conn.Controller("chat").On<string>("backtest", data => Console.WriteLine(data));
            Console.WriteLine("invoked");
            Console.ReadLine();
        }
    }
}
