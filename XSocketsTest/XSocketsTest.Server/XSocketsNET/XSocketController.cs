﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XSockets.Core.XSocket;
using XSockets.Plugin.Framework.Attributes;
using XSockets.Core.XSocket.Helpers;



namespace XSocketsTest.Server.XSocketsNET
{
    [XSocketMetadata(PluginAlias = "Chat")]
    public class Chat : XSocketController
    {
        public Chat()
        {

        }

        public void ChatMessage(ChatModel test)
        {

            // Send Message back:
            this.InvokeToAll("Hello to all from server", "backtest");
        }
    }

    public class ChatModel
    {
        public string Text { get; set; }
    }
}