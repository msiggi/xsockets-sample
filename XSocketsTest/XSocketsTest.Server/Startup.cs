﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using XSockets.Owin.Host;

[assembly: OwinStartup(typeof(XSocketsTest.Server.Startup))]

namespace XSocketsTest.Server
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseXSockets(true);

        }
    }
}
